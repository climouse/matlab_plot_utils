classdef barplot < handle
    
    methods (Static)
    function [Xs,hs,hs_eb]=bar(Y,Y_eb,cols,delta_in,delta_out,ms,eb_width,bar_width,pad)
    % all bars are of size 1
    iscol=1;
    %hs_eb=[];
    if nargin<3 | isempty(cols) 
        cols=[];
        iscol=0;
    end
    if nargin<2
        Y_eb=[];
    end
    if nargin<4
        delta_in=0;
    end
    if nargin<5
        delta_out=0;
    end
        
    if nargin<6
        ms=1;
    end
    
    if nargin<7
        eb_width=0.3;
    end
    if nargin<8
        bar_width=1;
    end
    [ncols1,ncols2]=size(cols);
    if not(iscell(Y))
        [nn,mm]=size(Y);
        Y=mat2cell(Y,nn,ones(1,mm));
    end
    if not(iscell(Y_eb))
        [nn,mm]=size(Y_eb);
        Y_eb=mat2cell(Y_eb,nn,ones(1,mm));
    end
    nout=length(Y);
    nin=cellfun(@(x) length(x),Y,'uni',1);
    nincum=cumsum(nin);
    
%     xs1=1:nincum(j)
    cx1=bar_width(1);
     hs={};
     hs_eb={};
     if length(bar_width)==1
         bar_width=bar_width*ones(1,nout);
     end
     
     if nargin<9
         pad=[bar_width(1)/2,bar_width(end)/2];
     end
     
    for j=1:nout
        x_rel=(0:nin(j)-1)'*(1+delta_in)*bar_width(j);
        x_abs=cx1+x_rel;
        cx1=bar_width(j)/2+x_abs(end)+delta_out(min(j,length(delta_out)));
        if j<nout
            cx1=cx1+bar_width(j+1)/2;
        end
        Xs{1,j}=x_abs;
        chs=[];
    
       
        for i=1:nin(j)
            cchs=bar(x_abs(i),Y{j}(i),bar_width(j))
            chs=cat(1,chs,cchs);
            hold all
            if iscol
                set(cchs,'FaceColor',cols{min(i,ncols1),min(j,ncols2)});
            end
                set(cchs,'EdgeColor','k','LineWidth',2);
           
                
        end
        hs=cat(2,hs,{chs});
    end
    
    Xms=cellfun(@(x) mean(x,1),Xs,'Uni',1);
    if ms
        set(gca,'XTick',Xms,'XTickLabel',[]);
    else
        Xs2=Xs';
        set(gca,'XTick',vertcat(Xs2{:}),'XTickLabel',[]);
    end
    
    cx=Xs{1,end};
    cx0=Xs{1,1};
    xlim([cx0(1)-bar_width(1)/2-pad(1),cx(end)+bar_width(end)/2+pad(2)]);
    
  if not(isempty(Y_eb))
    for j=1:nout
          
        x_abs=Xs{1,j};
        chs_eb=[];
        
        cY_eb=Y_eb{j};
        if not(isempty(cY_eb))
        for i=1:nin(j)
             
            ebm=0;
            ebp=0;
            if size(cY_eb,2)==2
                ebm=cY_eb(i,1);
                ebp=cY_eb(i,2);
            else
                 ebm=cY_eb(i);
                ebp=cY_eb(i);
            end
                cchs_eb_line=plot([x_abs(i),x_abs(i)],[Y{j}(i)-ebm,Y{j}(i)+ebp],'LineWidth',1,'Marker','none','Color','k');
                cchs_eb_bbar=plot([x_abs(i)-eb_width/2,x_abs(i)+eb_width/2],[Y{j}(i)-ebm,Y{j}(i)-ebm],'LineWidth',1,'Marker','none','Color','k');
                cchs_eb_tbar=plot([x_abs(i)-eb_width/2,x_abs(i)+eb_width/2],[Y{j}(i)+ebp,Y{j}(i)+ebp],'LineWidth',1,'Marker','none','Color','k');
                chs_eb=cat(1,chs_eb,[cchs_eb_line,cchs_eb_bbar,cchs_eb_tbar]);
                
%                 hs_eb{i,j}=errorbar([x_abs(i),cx(end)],[Y{j}(i),0],...
%                     [Y_eb{j}(i),0],'LineWidth',2,...
%                     'Color','k','LineStyle','none');
%             
                
        end
        else
            chs_eb={};
        end
        hs_eb=cat(2,hs_eb,{chs_eb});
        
    end
  end
    end
    
    
    
    function [Xs,hs,hs_eb]=test_bar()
        Y={[1,2,3]',[1,2]',[1,2,3,4,5]'};
Y_eb={[0.3,0.4,1]',[0.5,0.5]',[0.6,1,0.6,2,1]};
cols=[];
delta_in=0.2;
delta_out=1;
ms=1;
bar_width=0.3;
figure;
[Xs,hs,hs_eb]=barplot.bar(Y,Y_eb,cols,delta_in,delta_out,ms,bar_width);
set(hs{1},'FaceColor','r');
set(hs{2}(2),'EdgeColor','g','FaceColor',[1,1,1]);
set(hs_eb{2}(:,3),'Color','m');
set(gca,'XTickLabel',{'1','2','3'})
    end
    end
end
