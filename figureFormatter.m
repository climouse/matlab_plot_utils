classdef figureFormatter < handle
    properties
        figdir='';
        saveformats={'eps'};
        dim=struct('w',3,'h',3,'z',1);
        p_screen=struct('ppi',72,'defaultFigurePosition',[])
        p_axes=struct('FontName','arial','FontWeight','normal','FontSize',12,...
            'LineWidth',0.5,'Box','off');
        aflag=0;
        ratio=2;
        grey2black=1;
        label_sz=12;
        p_leg=struct('FontSize',24,'Box','off');
        doplot=2;
        dataname='';
        stack={};
        stack_desc={};
    end
    methods
        
        function obj=figureFormatter(project,papermode)
            if nargin<2
                papermode=1;
            end
            %p.displayZoom=1;
            
            def_axes_paper=struct('FontName','arial','FontWeight','normal','FontSize',12,...
                'LineWidth',0.5,'Box','off');
            def_axes_ppt=struct('FontName','arial','FontWeight','normal','FontSize',18,...
                'LineWidth',0.5,'Box','off');
            def_dim_paper=struct('w',3,'h',2,'z',1.2);
            def_dim_ppt=struct('w',3,'h',2,'z',2.5);
            
            obj.figdir=project;
            
            if papermode
                obj.dim=def_dim_paper;
                obj.p_axes=def_axes_paper;
            else
                obj.dim=def_dim_ppt;
                obj.p_axes=def_axes_ppt;
            end
            
            %if not(isfield(p,'screenppi')) | isempty(p.screenppi)
            screenppi=get(0,'ScreenPixelsPerInch');
            %end
            %if not(isfield(p,'defaultFigurePosition')) | isempty(p.defaultFigurePosition)
            defaultFigurePosition=get(0,'defaultFigurePosition');
            %end
            ppi=screenppi;
            obj.p_screen.ppi=screenppi;
            defpos=defaultFigurePosition;
            obj.p_screen.defaultFigurePosition= defpos;
            set(0,'defaultFigureInvertHardcopy','on'); % This is the default anyway
            set(0,'defaultFigurePaperUnits','inches'); % This is the default anyway
            
            %set(0,'defaultFigurePaperPosition', [defpos(1) defpos(2) obj.dim.w*obj.dim.z obj.dim.h*obj.dim.z]);
            %set(0,'defaultFigurePosition', [defpos(1) defpos(2) obj.dim.w*ppi*obj.dim.z obj.dim.h*ppi*obj.dim.z]);
            
            % Set the defaults for saving/printing to a file
            
        end
        
        function setdim(obj,whz)
            if isempty(whz)
                whz=[3,2,1];
            end
            obj.dim.w=whz(1);
            obj.dim.h=whz(2);
            obj.dim.z=whz(3);
        end
        
        function oldstack=clear(obj,clean)
            if nargin<2
                clean=0;
            end
            if clean
                for i=1:length(obj.stack)
                     cmd2run=['rm ' obj.stack{i} '.*'];
                fprintf(1,'Running command %s \n',cmd2run);
                system(cmd2run);
                end
            end 
            oldstack=obj.stack;
            obj.stack={};
            obj.stack_desc={};
        end
            
        
        function save(obj,figh,figname,keepformat)
            if nargin<4
                keepformat=0;
            end
            % keepformat: 0, swith and keep, 1=switch and revert, 2=don't
            % switch
            if keepformat<2
                [p_fig,ah,p_axes]=obj.formatfig(figh);
            end
            figureFormatter.asf(figh.Number,obj.figdir,figname,obj.saveformats,1); %1 is for autosize off
            if keepformat>0
                set(figh,p_fig);
                for i=1:length(ah)
                    %set(ah(i),p_axes(i){:});
                end
            end
        end
        
        function fig_fullname=savefig(obj,figh,figname,dim,doplot,desc)
            cdim=obj.dim;
            %obj.dim=dim;
            if nargin<5
                doplot=obj.doplot;
            end
            if doplot
                if nargin>3 & not(isempty(dim))
                obj.setdim(dim);
                [p_fig,ah,p_axes]=obj.formatfig(figh);
                end
                if doplot>1
                    fnames_save=figname;
                    if not(isempty(obj.dataname))
                        fnames_save=[obj.dataname '_' figname];
                    end
                    fig_fullname=figureFormatter.asf(figh.Number,obj.figdir,fnames_save,obj.saveformats,1); %1 is for autosize off
                    mydesc={''};
                    if nargin>5
                      mydesc={desc};
                    end
                    obj.stack=cat(1,obj.stack,{fig_fullname});
                     obj.stack_desc=cat(1,obj.stack_desc,mydesc);
                end
                obj.dim=cdim;
            end
        end
        
        function stack_2latex_aux(obj,scale,fname,template)
            % if template if 1, append
            % if template is 0, use default template and overwrite
            % otherwise use template file and overwrite
           
            fname_full=fullfile(obj.figdir,[obj.dataname '_' fname]);
            if exist(fname_full,'file') & not(ischar(template)) & (template) % append case
                tmpfile=fullfile(obj.figdir,'tmp2latex.tmp');
                tmpappend=fullfile(obj.figdir,'tmp_append.tex');
                fid=fopen(tmpfile,'w');
                
                for i=1:length(obj.stack)
                    if not(isempty(obj.stack_desc{i}))
                      fprintf(fid,'\\par\n%s\n\\par\n',obj.stack_desc{i});
                    end
                    fprintf(fid,'\\begin{center}\n');
                    myfile=strsplit(obj.stack{i});
                    fprintf(fid,'\\includegraphics[scale=%g]{%s}\n',scale,myfile{end});
                    fprintf(fid,'\\end{center}\n');
                end
                 
                fclose(fid);
                %cmd2run=['sed -n -i -e ''/\end{document}/r ' tmpfile ''' -e 1x -e ''2,${x;p}'' ' fname_full];
                %cmd2run=['sed -e ''N;/\n.*\end{document}/{r ' tmpfile ''' -e ''};P;D'' ' fname_full ' > ' tmpappend];
                cmd2run=['awk ''/\end{document}/{while(getline line<"' tmpfile '"){print line}} //'' ' fname_full ' > ' tmpappend];
                fprintf(1,'Running command %s \n',cmd2run);
                system(cmd2run);
                cmd2run=['mv ' tmpappend ' ' fname_full];
                fprintf(1,'Running command %s \n',cmd2run);
                system(cmd2run);
                
            else
            
            if ischar(template) % we use template
                if not(exist(template,'file'))
                    error('template file not found');
                else
                 cmd2run=['cat ' template ' > ' fname_full];
                 fprintf(1,'Running command %s \n',cmd2run);
                 system(cmd2run);
                 stack_2latex_aux(obj,scale,fname,1);
                 
                end
            else % we use default template
                deftemplate=fullfile(obj.figdir,'deftemplate.tex');
                fid=fopen(deftemplate,'w');
                fprintf(fid,'\\documentclass{article}\n');
                fprintf(fid,'\\usepackage{graphicx}\n');
                fprintf(fid,'\\usepackage[margin=0.5in]{geometry}\n');
%                 fprintf(fid,'\\usepackage[outdir=./]{epstopdf}\n');
                
                fprintf(fid,'\\begin{document}\n');
                fprintf(fid,'\\pagestyle{empty}\n');
                fprintf(fid,'\\end{document}\n');
                fprintf(fid,'%%autogenerated template\n');
                fclose(fid);
                stack_2latex_aux(obj,scale,fname,deftemplate);
                
            end
            end
            
            
        end
        
        function stack_2latex(obj,scale,fname,template,compile_flag,open_flag)
            cdir=pwd;
             if not(isempty(obj.stack))
                fprintf(1,'Dumping stack to latex document: %g figures\n',length(obj.stack));
            
            fname_full=fullfile(obj.figdir,[obj.dataname '_' fname]);
            obj.stack_2latex_aux(scale,fname,template);
            cd(obj.figdir);
            if compile_flag
                cmd2run=(['latexmk -pdfps ' fname_full ' && latexmk -c ' fname_full]);
                if open_flag
                    cmd2run=[cmd2run ' && echo "Now opening file" && open -a /Applications/Skim.app/Contents/MacOS/Skim ' fname_full(1:end-3) 'pdf'];
                end
                fprintf(1,'Running command %s \n',cmd2run);
                 system(cmd2run,'-echo');
                 
            end
            if not(compile_flag) & open_flag
                 system(['open -a /Applications/Skim.app/Contents/MacOS/Skim ' fname_full(1:end-3) 'pdf']);
            end
            cd(cdir);
             else
                 fprintf(1,'Stack empty, nothing to dump\n');
             end
        
        end
        function savevar(obj,varlist,filename)
            
            if not(isempty(obj.dataname))
                fname_save=fullfile(obj.figdir,[obj.dataname '_' filename]);
                
            else
                fname_save=fullfile(obj.figdir,filename);
            end
            fprintf(1,'Saving data in %s\n',fname_save);
            save(fname_save,varlist{:});
            
        end
        function [p_fig,cax,p_axes]=formatfig(obj,figh)
            ppi=obj.p_screen.ppi;
            p_fig=get(figh);
            %set(figh,'Units','inches');
            
            cax=findobj(figh,'Type','axes'); %CurrentAxes');
            for i=length(cax):-1:1
                p_axes(i,1)=get(cax(i));
                set(cax(i),obj.p_axes);
                set(cax(i),'TickLength',get(cax(i),'TickLength')*obj.ratio);
                ccolx=get(cax(i),'XColor');
                ccoly=get(cax(i),'YColor');
                if obj.label_sz>-1
                    xl=get(cax(i),'XLabel');
                    yl=get(cax(i),'YLabel');
                    set(xl,'FontSize',obj.label_sz);
                    set(yl,'FontSize',obj.label_sz);
                end
                if obj.grey2black & length(unique(ccolx))==1
                    set(cax(i),'XColor',[0,0,0]);
                end
                if obj.grey2black & length(unique(ccoly))==1
                    set(cax(i),'YColor',[0,0,0]);
                end
                
                
            end
            cleg=findobj(figh,'Type','legend');
            set(cleg,obj.p_leg);
            %paperpos=get(figh,'PaperPosition');
            cpos=get(figh,'PaperPosition');
            set(figh,'PaperPositionMode','manual');
            %set(figh,'PaperPosition',[paperpos(1:2),obj.dim.w*obj.dim.z,obj.dim.h*obj.dim.z]);
            
            
            %set(cax(1),'LooseInset',[0,0,0,0]);
            if not(obj.aflag)
                set(figh,'PaperPosition',[cpos(1),cpos(2),obj.dim.w*obj.dim.z,obj.dim.h*obj.dim.z]);
            else
                
                
                set(cax(1),'LooseInset',[0,0,0,0]);
                
                
                for i=1:length(cax)
                    
                    caxpos=get(cax(i),'Position');
                    set(cax(i),'ActivePositionProperty','OuterPosition');
                    set(cax(i),'Units','inches');
                    set(cax(i),'Position',[caxpos(1),caxpos(2),obj.dim.w*obj.dim.z,obj.dim.h*obj.dim.z]);
                    set(cax(i),'Units','pixels');
                    caxposo=get(cax(i),'OuterPosition'); %+get(cax,'TightInset');
                    set(cax(i),'OuterPosition',[1 1 caxposo(3) caxposo(4)]);
                    caxposo=get(cax(i),'OuterPosition');
                end
                set(figh,'Units','pixels');
                cpos=get(figh,'Position');
                set(figh,'Position',[cpos(1) cpos(2) caxposo(3)+1 caxposo(4)+1]);
                %set(gca, 'Position', get(gca, 'OuterPosition') - ...
                %    get(gca, 'TightInset') * [-1 0 1 0; 0 -1 0 1; 0 0 1 0; 0 0 0 1]);
                %set(figh,'Units','inches');
                set(cax,'Units','normalized');
                for i=length(cax):-1:1;
                    %p_axes(i,1)=get(cax(i));
                    % set(cax(i),obj.p_axes);
                end
            end
            
            
            %set(findall(figh,'-property','FontSize'),'FontSize',12)
            % set(gca, 'Position', get(gca, 'OuterPosition') - ...
            %     get(gca, 'TightInset') * [-1 0 1 0; 0 -1 0 1; 0 0 1 0; 0 0 0 1]);
            
            
            
            %set(figh,'PaperPosition',
        end
        
    end
    methods (Static)
        function figname=asf(fignum,figfolder,tag,figtype,notimetag,autosizeoff)
            
            if nargin<6
                autosizeoff=0;
            end
            
            deffigtype={'svg','fig'};
            
            
            if nargin<5
                notimetag=0;
            end
            if nargin<4 | isempty(figtype)
                figtype=deffigtype;
            end
            if not(iscell(figtype))
                figtype={figtype};
            end
            
            if nargin<3 | isempty(tag)
                tag='';
            end
            if nargin<2 | isempty(figfolder)
                figfolder=pwd; %undefined project
            end
            %check folder exists
            fprintf(1,'Saving figure to folder : %s \n',figfolder);
            if not(exist(figfolder,'dir'))
                error(sprintf('Folder %s does not exist',figfolder));
            end
            
            
            for i=1:length(fignum)
                daystr=datestr(now,'yymmdd-HHMMSS');
                if notimetag
                    daystr='';
                else
                    daystr=[daystr '_'];
                end
                figname=fullfile(figfolder,['fig_' daystr tag]);
                if not(autosizeoff)
                    set(fignum(i),'PaperPositionMode','auto')
                end
                
                for j=1:length(figtype)
                    figext=figtype{j};
                    saveparams='';
                    cfigext='';
                    switch figext
                        case 'fig'
                            cfigname=[figname '.' figext];
                            saveas(fignum(i),cfigname);
                            fprintf(1,[cfigname '\n']);
                            saveparams='';
                        case 'pdf'
                            saveparams='''-painters'',''-r300'',''-dpdf''';
                            cfigext='.pdf';
                        case 'eps'
                            saveparams='''-painters'',''-r0'',''-depsc2'',''-tiff''';
                            cfigext='.eps';
                        case 'eps3'
                            saveparams='''-painters'',''-r0'',''-depsc2''';
                            cfigext='.eps';
                        case 'png'
                            saveparams='''-r300'',''-dpng''';
                            cfigext='.png';
                        case 'svg'
                            saveparams='''-painters'',''-r300'',''-dsvg''';
                            cfigext='.svg';
                        case 'tiff'
                            saveparams='''-r300'',''-dtiff''';
                            cfigext='.tiff';
                        otherwise
                            saveparams=figext;
                            cfigext='';
                    end
                    if not(isempty(saveparams))
                        
                        cfigname=[figname cfigext];
                        %saveparams
                        str2eval=['print(' num2str(fignum(i)) ',''' cfigname ''',' saveparams ')'];
                        fprintf(1,'Eval command : %s \n',str2eval);
                        eval(str2eval);
                        if strcmp(figtype{j},'eps3')
                            
                            cfigname2=['''' cfigname ''''];
                            sep='''';
                            str2eval=['awk ''/^(0 0).*(re)$/ {$0="%"$0}1'' ' cfigname2 ' > ./tmp.csv && mv ./tmp.csv ' cfigname2];
                            
                            fprintf(1,'Eval command for awk: %s \n',str2eval);
                            system(str2eval);
                            %             eval(str2eval);
                        end
                        
                    end
                    
                end
            end
            
        end
        
        function [P,sz]=place_rectangles(M,centering,pads)
            w=max(M(:,:,1),[],1); %row/horiz vector
   
            h=max(M(:,:,2),[],2); %col/vert vector
   
            w_cum=cumsum(w); 
        
            h_cum=cumsum(h);
            sz=[w_cum(end),h_cum(end)];
            flp=1;
            
            P=nan(size(M));
            [m,n,~]=size(M);
            P(:,:,1)=repmat(w_cum,m,1)-repmat(w,m,1)/2.*(1-centering(:,:,1));
            P(:,:,2)=repmat(h_cum,1,n)-repmat(h,1,n)/2.*(1-(-2*flp+1)*centering(:,:,2));
             P(:,:,1)=P(:,:,1)-1/2*M(:,:,1).*(1+centering(:,:,1));
             P(:,:,2)=P(:,:,2)-1/2*M(:,:,2).*(1+(-2*flp+1)*centering(:,:,2));
            if flp
            P(:,:,2)=sz(2)-(P(:,:,2)+M(:,:,2));
            end
            
            if nargin>2 & not(isempty(pads))
                if numel(size(pads))==2
                    pads=cat(3,pads(1)*ones(m,n),pads(2)*ones(m,n),pads(3)*ones(m,n),pads(4)*ones(m,n));
                end
                deltasW=cat(2,pads(:,1,4),-diff(P(:,:,1),1,2)+M(:,1:end-1,1)+pads(:,1:end-1,2)+pads(:,2:end,4));
                deltasH=cat(1,diff(P(:,:,2),1,1)+M(2:end,:,2)+pads(1:end-1,:,1)+pads(2:end,:,3),pads(end,:,1)); %+h2+B1+T2
                
                deltasW_max=max(0,max(deltasW,[],1)); %horz vector
                deltasH_max=max(0,max(deltasH,[],2)); %vert vector
                
                deltasW_cum=cumsum(deltasW_max);
                deltasH_cum=flipud(cumsum(flipud(deltasH_max)));
                P(:,:,1)=P(:,:,1)+repmat(deltasW_cum,m,1);
                P(:,:,2)=P(:,:,2)+repmat(deltasH_cum,1,n);
                sz(1)=max(P(:,end,1)+M(:,end,1)+pads(:,end,2),[],1);
                sz(2)=max(P(1,:,2)+M(1,:,2)+pads(1,:,3),[],2);
            end
                
           
            
           
        end
        function compfig=compose(figs,centering,useax,unt,clz,pads_BRTL)
            [m,n]=size(figs);
            if nargin<2
                centering=zeros(m,n,2);
            else
                sz0=size(centering);
                if numel(sz0)==2
                    centering=cat(3,centering(1)*ones(m,n),centering(2)*ones(m,n));
                end
            end
                
            if nargin<3
              
                useax=0;
            end
            s='OuterPosition';
            if useax
                s='Position';
            end
            if nargin<4
                unt='normalized';
            end
            if nargin<5
                clz=0;
            end
      
            if nargin<6
                pads_BRTL=[];
                if useax
                    pads_BRTL=[0,0,0,0];
                end
            end
            if not(isa(figs,'cell'))
                figs=num2cell(figs);
                for i=1:m
                    for j=1:n
                        if isnan(figs{i,j})
                            figs{i,j}=[];
                        else
                            figs{i,j}=figure(figs{i,j});
                        end
                    end
                end
            end
            
            M=zeros(m,n,2);
          
            if useax
                pads=zeros(m,n,4);
            else
                pads=pads_BRTL;
            end
            for i=1:m
                for j=1:n
                    
                    
                    if isempty(figs{i,j})
                        M(i,j,1)=0;
                        M(i,j,2)=0;
                    else
                        cfig=figs{i,j};
                        set(cfig,'Units','points');
                        ax=findobj(cfig,'type','axes');
                        ax_pos=[0,0,0,0];
                        ax_outpos=[0,0,0,0];
                        for k=1:length(ax)
                            set(ax(k),'Units','points');
                            ax_pos=cat(1,ax_pos,get(ax(k),s));
                            ax_outpos=cat(1,ax_outpos,get(ax(k),'OuterPosition'));
                        end
                        ax_wh_max=[max(ax_pos(:,3)),max(ax_pos(:,4))];
                        %ax_outwh_max=[max(ax_outpos(:,3)),max(ax_outpos(:,4))];
                        M(i,j,1)=ax_wh_max(1);
                        M(i,j,2)=ax_wh_max(2);
                        if useax
                            %uses the first axes
                            pads(i,j,1)=ax_pos(2,2)-ax_outpos(2,2)+pads_BRTL(1);
                            pads(i,j,2)=ax_outpos(2,1)+ax_outpos(2,3)-ax_pos(2,1)-ax_pos(2,3)+pads_BRTL(2);
                            pads(i,j,4)=ax_pos(2,1)-ax_outpos(2,1)+pads_BRTL(4);
                            pads(i,j,3)=ax_outpos(2,4)+ax_outpos(2,2)-ax_pos(2,4)-ax_pos(2,2)+pads_BRTL(3);
                        end
                        
                    end
                end
            end
            
            [P,sz]=figureFormatter.place_rectangles(M,centering,pads);
            
            compfig=figure;
            compfig_pos=get(compfig,'Position');
            set(compfig,'Position',[compfig_pos(1:2),sz]);
            for i=1:m
                for j=1:n
                    if isempty(figs{i,j})
          
                    else
                        cfig=figs{i,j};
                        
                        ax=findobj(cfig,'type','axes');
                      %  lgs=findobj(cfig,'type','legend');
                        for k=1:length(ax)
                            %set(ax(k),'Units','points');
                            
                            ax_new=copyobj(ax(k),compfig);
                            cpos=get(ax_new,s);
                            set(ax_new,s,[P(i,j,1),P(i,j,2),cpos(3),cpos(4)]);
                            set(ax_new,'Units',unt);
                        end
                        if clz
                            close(cfig);
                        end
                       
                    end
                end
            end
                            
                            
                            
                       
           
                        
            
        end
            function sb=make_scalebar(ax,dxdy,xy)
                if nargin<3
                    xlims=get(gca,'XLim');
                    ylims=get(gca,'YLim');
                    xy=[xlims(1),ylims(1)];
                end
                sb=line(ax,[xy(1),xy(1),xy(1)+dxdy(1)],[xy(2)+dxdy(2),xy(2),xy(2)],'Color','k');
                
            end             
                        
            function cols=getDefaultColors(n,colset)
            
            %%
        
            defaultColorder={ [0    0.4470    0.7410],...
                [0.8500    0.3250    0.0980],...
                [0.9290    0.6940    0.1250],...
                [0.4940    0.1840    0.5560],...
                [0.4660    0.6740    0.1880],...
                [0.3010    0.7450    0.9330],...
                [0.6350    0.0780    0.1840]};
            if nargin<2
               colset='default';
            end
            switch(colset)
               case 'default'
               case 'old'
            end
                
            cols=repmat({[0,0,0]},n,1);
            for i=1:n
                cols{i}=defaultColorder{mod(i-1,length(defaultColorder))+1};
            end
            
            %%
        end
        
    end
    
    
end
